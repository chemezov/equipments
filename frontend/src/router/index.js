import Vue from 'vue';
import Router from 'vue-router';
import CallCenter from '@/components/CallCenter';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'CallCenter',
      component: CallCenter,
    },
  ],
  mode: 'history',
});
