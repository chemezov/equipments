import os
import logging
from logging.handlers import RotatingFileHandler
from flask import Flask, render_template
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from flask import Flask, render_template, jsonify

db = SQLAlchemy()
migrate = Migrate()


def create_app(config_class=Config):
    sentry_sdk.init(
    dsn="http://75b93dff26e848cdb222b8e7de4bc7f4@134.209.207.213:9000/2",
    integrations=[FlaskIntegration()]
    )
    app = Flask(__name__, static_folder='../static/static', template_folder='../static')
    app.config.from_object(config_class)

    # enable CORS
    CORS(app, resources={r'/*': {'origins': '*'}})

    db.init_app(app)
    migrate.init_app(app, db)

    @app.route('/status', methods=['GET'])
    def healthcheck():
            # Telegraf logic:
            # 0 code - healthy
            # non zero - unhealthy
            try:
                db.session.execute('SELECT id FROM Car LIMIT 1')
                return jsonify({
                    "app_status": "ok",
                    "database_status": "ok",
                    "app_status_code": 0,
                    "database_status_code": 0})
            except:
                return jsonify({
                    "app_status": "ok",
                    "database_status": "fail",
                    "app_status_code": 0,
                    "database_status_code": 1})

    @app.route('/', defaults={'path':''})
    @app.route('/<path:path>')
    def catch_all(path):
        return render_template("index.html")

    from app.api import bp as api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    if not app.debug and not app.testing:
        if not os.path.exists('logs'):
            os.mkdir('logs')
        file_handler = RotatingFileHandler('logs/car-api.log', maxBytes=10240, backupCount=10)
        file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d'))
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)

        app.logger.setLevel(logging.INFO)
        app.logger.info('CAR-API startup')

    return app

